<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'HomeController@home');

//Route::get('/register', 'AuthController@register');

//Route::get('/welcome/{id}', 'AuthController@welcome');
//Route::post('/welcome', 'AuthController@welcome');

/*route::get('/master', function() {
    return view('/layouts.master');
});*/

/*Route::get('/', function() {
    return view('/tables.index');
});*/

/*Route::get('/data-tables', function() {
    return view('/tables.data-tables');
});*/


Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');