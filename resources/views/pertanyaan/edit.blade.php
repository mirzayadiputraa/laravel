@extends('layouts.master')
@section('title', 'Edit Pertanyaan')
@section('content')
<div class="mt-2 ml-3">
    <h2>Edit Pertanyaan {{$pertanyaan->id}}</h2>
    <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Judul</label>
            <input type="text" class="form-control" name="judul" value="{{$pertanyaan->judul}}" id="title" placeholder="Masukkan Judul">
            @error('title')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Isi</label>
            <input type="text" class="form-control" name="isi"  value="{{$pertanyaan->isi}}"  id="body" placeholder="Edit Pertanyaan">
            @error('body')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
    
@endsection
