@extends('layouts.master')
@section('title', 'Tambah pertanyaan')
@section('content')
<div class="ml-3 mt-2">
    <h2>Masukkan Pertanyaan</h2>
            <form action="/pertanyaan" method="POST">
                @csrf
                <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" class="form-control" name="judul" id="title" placeholder="Masukkan Judul">
                    @error('title')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">Isi</label>
                    <textarea class="form-control" name="isi" id="body" cols="30" rows="10" placeholder="Tuliskan Pertanyaan"></textarea>
                    @error('body')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Create</button>
            </form>
        </div>
</div>
@error("body")
<div class=”alert alert-danger”>
{{ $message }}
</div>
@enderror
    
@endsection